<?php

use Sda\Lights\Config\Config;
use Sda\Lights\Controller\ApiController;
use Sda\Lights\Db\DbConnection;
use Sda\Lights\Request\Request;
use Sda\Lights\Response\Response;

require_once(__DIR__ . '/../vendor/autoload.php');

$api = new ApiController(
    new Request(),
    new Response(),
    new DbConnection(Config::$connectionParams)
);

$api->run();