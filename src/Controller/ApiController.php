<?php

namespace Sda\Lights\Controller;

use Sda\Lights\Db\DbConnection;
use Sda\Lights\Light\LightFactory;
use Sda\Lights\Light\LightFactoryException;
use Sda\Lights\Light\LightNotFoundException;
use Sda\Lights\Light\LightRepository;
use Sda\Lights\Request\Request;
use Sda\Lights\Response\Response;
use Sda\Lights\Routing\Routing;

/**
 * Class ApiController
 * @package Sda\Lights\Controller
 */
class ApiController
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Response
     */
    private $response;
    /**
     * @var LightRepository
     */
    private $lightRepository;

    /**
     * ApiController constructor.
     * @param Request $request
     * @param Response $response
     * @param DbConnection $connection
     */
    public function __construct(
        Request $request,
        Response $response,
        DbConnection $connection
    )
    {
        $this->request = $request;
        $this->response = $response;

        $this->lightRepository = new LightRepository($connection);
    }

    public function run()
    {
        $action = $this->request->getParamFormGet('action');

        switch ($action) {
            case Routing::LIGHT:
                $id = $this->request->getParamFormGet('id', 0);

                if (0 === $id) {
                    $this->response->send404();
                }

                if(Request::HTTP_METHOD_GET === $this->request->getHttpMethod()){
                    $this->getLight($id);
                }elseif(Request::HTTP_METHOD_POST === $this->request->getHttpMethod()) {


                    $this->setLight($id);
                }else{
                    $this->response->send('', Response::STATUS_400_BAD_REQUEST);
                }
                $this->getLight($id);
                break;
            case  Routing::GET_ALL_LIGHTS:
                break;
            default:
                $this->response->send404();
                break;
        }
    }

    private function getLight($id)
    {


        try {
            $light = $this->lightRepository->getLight($id);
            $this->response->send($light);
        } catch (LightNotFoundException $e) {
            $this->response->send404();
        }

    }

    private function setLight($id)
    {
        $inputData =  $this->request->getJsonRequestBody();

        try{
            $light = LightFactory::makeWithId($id, $inputData);
            if(false === $light->validate()){
                $this->response->send('Incorrect state', Response::STATUS_400_BAD_REQUEST);
            }
            $this->lightRepository->save($light);
        }catch (LightFactoryException $e){
            $this->response->send('Undefined state param in request', Response::STATUS_400_BAD_REQUEST);
        }


    }

}