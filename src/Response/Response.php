<?php

namespace Sda\Lights\Response;

/**
 * Class Response
 * @package Sda\Lights\Response
 */
class Response
{
    const STATUS_OK = '200';
    const STATUS_400_BAD_REQUEST = '400';
    const STATUS_404_NOT_FOUND = '404';

    public function  send404()
    {
        $this->send('', self::STATUS_404_NOT_FOUND);
    }

    /**
     * @param string $data
     * @param string $status
     */
    public function send($data = '', $status = self::STATUS_OK)
    {
        http_response_code($status);
        echo json_encode($data);
        $this->stopApi();
    }

    public function  stopApi()
    {
        die();
    }
}