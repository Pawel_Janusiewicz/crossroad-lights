<?php
/**
 * Created by PhpStorm.
 * User: RENT
 * Date: 2016-07-28
 * Time: 19:01
 */

namespace Sda\Lights\Routing;


/**
 * Class Routing
 * @package Sda\Lights\Routing
 */
class Routing
{
    const LIGHT = 'light';
    const GET_ALL_LIGHTS = 'lights';
}