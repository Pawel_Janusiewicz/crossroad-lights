<?php

namespace Sda\Lights\Light;

/**
 * Class Light
 * @package Sda\Lights\Light
 */
class Light implements \JsonSerializable
{

    const STATE_SHUTDOWN = 'shutdown';
    const STATE_SUSPENDED = 'suspended';
    const STATE_GREEN = 'green';
    const STATE_RED = 'red';
    const STATE_YELLOW = 'yellow';
    const STATE_RED_YELLOW = 'red_yellow';
    const DEFAULT_STATE = self::STATE_SUSPENDED;

    public static $availableStates = [
      self::STATE_SHUTDOWN,
        self::STATE_SUSPENDED,
        self::STATE_GREEN,
        self::STATE_RED,
        self::STATE_YELLOW,
        self::STATE_RED_YELLOW
    ];


    /**
     * Light constructor.
     * @param int $id
     * @param string $state
     * @param LightValidator $validator
     */
    public function  __construct(
        $id,
        $state,
        LightValidator $validator
    )
    {
        $this->id = $id;
        $this->state = $state;
        $this->validator = $validator;
    }

    /**
     * @return bool
     */
    public function validate()
    {
        return $this->validator->validateLight($this);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    public function jsonSerialize()
    {
        return [
          'id' => $this->id,
          'state' => $this->state
        ];
    }
}