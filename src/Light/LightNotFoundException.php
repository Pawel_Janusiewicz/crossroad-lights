<?php

namespace Sda\Lights\Light;


/**
 * Class LightNotFoundException
 * @package Sda\Lights\Light
 */
class LightNotFoundException extends \Exception
{

}