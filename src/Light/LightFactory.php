<?php

namespace Sda\Lights\Light;

/**
 * Class LightFactory
 * @package Sda\Lights\Light
 */
class LightFactory
{
    public static function makeWithId($id, array $param)
    {
        if (false === array_key_exists('state', $param)) {
            throw new LightFactoryException('Incorrect light params');
        }
        $builder = new LightBuilder();

        return $builder
            ->withId($id)
            ->withState($param['state'])
            ->build();
    }
    /**
     * @param array $param
     * @return Light
     * @throws LightFactoryException
     */
    public static function makeFromArray(array $param)
    {
        if (false === array_key_exists('id', $param) ||
        false === array_key_exists('state', $param)){
            throw new LightFactoryException('Incorrect light params');
        }
        $builder = new LightBuilder();

        return $builder
            ->withId($param['id'])
            ->withState($param['state'])
            ->build();

    }
}