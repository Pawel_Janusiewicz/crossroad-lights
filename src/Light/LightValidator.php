<?php

namespace Sda\Lights\Light;


/**
 * Class LightValidator
 * @package Sda\Lights\Light
 */
class LightValidator
{
    /**
     * @param Light $light
     */
    public function validateLight(Light $light)
    {
        return in_array($light->getState(), Light::$availableStates, true);
    }
}