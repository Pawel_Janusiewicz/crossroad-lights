<?php

namespace Sda\Lights\Light;


use Sda\Lights\Db\DbConnection;

/**
 * Class LightRepository
 * @package Sda\Lights\Light
 */
class LightRepository
{
    /**
     * @var DbConnection
     */
    private $connection;

    /**
     * LightRepository constructor.
     * @param DbConnection $connection
     */
    public function __construct(
        DbConnection $connection
    )
    {
        $this->connection = $connection;
    }


    /**
     * @param int $id
     * @return Light
     * @throws LightNotFoundException
     */
    public function getLight($id)
    {
        $query = $this->connection->getConnection()->createQueryBuilder();

        $sth = $query
            ->select('*')
            ->from('lights')
            ->where('id = ?')
            ->setParameter(0, $id)
            ->execute()
            ;

        $data = $sth->fetch(\PDO::FETCH_ASSOC);

        if(false === $data){
            throw new LightNotFoundException();
        }

        return LightFactory::makeFromArray($data);

    }

    public function save(Light $light)
    {
        $query = $this->getQueryBuilder();
        $query
            ->update('lights', 'l')
            ->set('l.state', ':state')
            ->where('l.id = :id')
            ->setParameter('state', $light->getState())
            ->setParameter('id', $light->getId())
            ->execute()
            ;

    }
    private function getQueryBuilder(){
        return $this->connection->getConnection()->createQueryBuilder();
    }
}